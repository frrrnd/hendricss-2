<!--<p align="center">
  <img src="" width="200">
</p>-->

# Hendricss 2

Boilerplate feito com BrowserSync, Stylus, Pug e ES6.


## Getting Started

### Installation

Instale as dependências do boilerplate.

- [NodeJS](http://nodejs.org/)
- [GulpJS](http://gulpjs.com/)


```sh
# instale o gulp global
$ npm install -g gulp

# instale as dependências
$ npm install

```

© Fernando Soares
