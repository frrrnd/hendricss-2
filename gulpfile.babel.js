import gulp from 'gulp';
import data from 'gulp-data';
import sourcemaps from 'gulp-sourcemaps';
import stylus from 'gulp-stylus';
import browserSync from 'browser-sync';
import watch from 'gulp-watch';
import pug from 'gulp-pug';
import fs from 'fs-extra';
import rupture from 'rupture';
import babel from 'gulp-babel';

/*
    Browser sync configs.
*/
gulp.task('browser-sync', ['stylus', 'pug', 'ES6'], () => {
    browserSync.init({
        server: {
            baseDir: "./build"
        }
    })
});

/*
    Compile Pug
*/

gulp.task('pug', () => gulp.src('./src/views/**/*.pug')
    .pipe(data(function(file) {
        return JSON.parse(
            fs.readFileSync('./src/data/config.json')
        );
    }))
    .pipe(pug({pretty: '\t'}))
    .pipe(gulp.dest('./build'))
    .pipe(browserSync.reload({stream: true})));


/*
    Compile and compress styl files.
*/

gulp.task('stylus', () => gulp.src('./src/assets/css/**/*.styl')
    .pipe(sourcemaps.init())
    .pipe(stylus({
        compress: true,
        use: [
            rupture()
        ]
    }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./build/assets/css/'))
    .pipe(browserSync.reload({stream: true})));

/*
    ES6
*/

gulp.task('ES6', () => {
    return gulp.src('./src/assets/javascripts/main.js')
    .pipe(babel({
        presets: ['es2015'],
        minified: true
    }))
    .pipe(gulp.dest('./build/assets/javascripts/'))
    .pipe(browserSync.reload({stream: true}))
})

gulp.task('test', () =>
    console.log("Foi")
)

/*
    Watch for modifications.
*/
gulp.task('watch', () => {
    gulp.watch('./src/assets/css/**/*.styl', ['stylus']);
    gulp.watch('./src/views/**/*.pug', ['pug']);
    gulp.watch('./src/assets/javascripts/*.js', ['ES6'])
});

gulp.task('default', ['browser-sync', 'watch']);

// const gulp = require('gulp');
// const data = require('gulp-data');
// const sourcemaps = require('gulp-sourcemaps');
// const stylus = require('gulp-stylus');
// const browserSync = require('browser-sync').create();
// const watch = require('gulp-watch');
// const pug = require('gulp-pug');
// const fs = require('fs-extra');
// const rupture = require('rupture');
// const babel = require('gulp-babel');
